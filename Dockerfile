FROM golang:latest
WORKDIR /app

# Download necessary Go modules
COPY go.mod ./
COPY go.sum ./
RUN go mod dowload
COPY *.go ./
RUN go build -o /docker-simple-test
EXPOSE 8080
CMD [ "/docker-simple-test" ]